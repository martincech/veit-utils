(defproject veit-utils "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [cheshire "5.4.0"]
                 [clj-http "2.3.0"]
                 [alandipert/intension "1.0.0"]
                 [datascript "0.15.5"]
                 [slingshot "0.12.2"]]
                )
