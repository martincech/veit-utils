(ns veit-utils.core
  (:require [clj-http.client :as http]
            [clojure.string :refer [split]]
            [slingshot.slingshot :refer [try+]]))

(def ACTION-PARAM "action")
(def GET-DB-SCHEMA-ACTION "getDbSchema")
(def GET-QUERY-RESULT-ACTION "getQueryResult")
(def ABRA-TEST-API-URL "http://intranet.veit.cz/ARESTD/API/RUN")

;; TODO: use intension to return queryable data structure?
;; First attemps resulted in a really slow execution
(defn get-abra-tables
  "Lists abra tables"
  ([] (get-abra-tables ABRA-TEST-API-URL))
  ([abra-api-url]
   (->  (http/get abra-api-url {:query-params {ACTION-PARAM GET-DB-SCHEMA-ACTION}
                                :as :json})
        :body)))

(defn get-table
  "Find table by exact name.
   You can use 2-arg version if you already have tables loaded in local var."
  ([table-name]
   (get-table (get-abra-tables) table-name))
  ([tables table-name]
   (first (filter #(= (:table_name %) (clojure.string/upper-case table-name)) tables))))

(defn get-table-by-prefix
  "Similar to `get-table` but find all possible matches by checking if table name starts with given prefix.
   Unlike `get-table` this function returns collection even if it returns only single table!"
  ([table-name]
   (get-table-by-prefix (get-abra-tables) table-name))
  ([tables table-name]
   (filter #(.startsWith (:table_name %) table-name) tables)))

(defn print-table-columns
  "Print each table column on a new line.
   You can pass existing tables structure - in that case no api is called.
   Otherwise, the `get-abra-tables` is called internally."
  ([table-name]
   (print-table-columns (get-abra-tables) table-name))
  ([tables table-name]
   (let [table (get-table tables table-name)]
     (doseq [col (:columns table)] (println (:column_name col))))))

(defn get-query-result
  "Executes given SQL query via internal ABRA API action GETSQLQUERY and returns
   result a sequence of rows"
  ([query]
   (get-query-result ABRA-TEST-API-URL query))
  ([abra-api-url query]
   (try+
    (-> (http/get abra-api-url {:query-params {ACTION-PARAM GET-QUERY-RESULT-ACTION
                                               :q query}})
        :body
        (split #"\r\n"))
    (catch [:status 500] {:keys [headers body]}
      (println "ERROR! check your SQL query: " body)))))

(defn print-query-result
  "Helper function for pretty printing the result of `get-query-result` function."
  ([query]
   (print-query-result ABRA-TEST-API-URL query))
  ([abra-api-url query]
   (let [result (get-query-result abra-api-url query)]
     (doseq [r result]
       (println r)))))


;;; Useful shortcuts
(def pq print-query-result)
;; you have to define global var 'tables to be able to use these functions!
;; like this: (def tables (get-abra-tables))
(defn t [tables table-name] (get-table tables table-name))
(defn ptc [tables table-name] (print-table-columns tables table-name))


