# veit-utils

Utility functions for interaction with VEIT / ABRA APIs.

## Usage

### Run REPL

```
lein repl

;; you can use `:reload-all` if you want to update the source code without a need to restart repl
(require '[veit-utils.core :refer :all] :reload-all])
```

### Run arbitrary SELECT

This is great for exploration and quick debugging of errors in your SELECTs.
Much quicker than manually upgrading ABRA API script and reloading every time you wanna try something.

```
;; Prints first 10 rows from SPMNORMS table - each row on a new line
(print-query-result "SELECT * FROM SPMNORMS ROWS 10")
```

```
;; Return result of given SELECT as a sequence of rows
(get-query-result "SELECT * FROM SPMNORMS ROWS 10")
```

### Load ABRA tables

```
(def tables (get-abra-tables))
```

### Get details about single table

```
;; get table by exact name
(get-table tables "SPMNORMS")

;; get all tables with name starting with given prefix
(get-table-by-prefix tables "SPMNORMS")

;; print all columns of given table in a human-readable form
(print-table-columns tables "SPMNORMS")
```
